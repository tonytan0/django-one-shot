from django.urls import path

from todos.views import TodoListView, TodoListDetailView, TodoListCreateView, TodoListUpdateView, TodoListDeleteView, TodoItemCreateView, TodoItemUpdateView



# TodoItem

urlpatterns = [
    path("", TodoListView.as_view(), name="todos_list"),
    path("<int:pk>/", TodoListDetailView.as_view(), name="todos_detail"),
    path("create/", TodoListCreateView.as_view(), name="todos_create"),
    path("<int:pk>/edit/", TodoListUpdateView.as_view(), name="todos_update"),
    path(
        "<int:pk>/delete/",
        TodoListDeleteView.as_view(),
        name="todo_delete",
    ),
    path(
        "items/create/",
        TodoItemCreateView.as_view(),
        name="item_create",
    ),
    path(
        "items/<int:pk>/edit/",
        TodoItemUpdateView.as_view(),
        name="item_update",
    ),
]
